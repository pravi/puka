/* eslint-disable */
'use strict';

const spawn = require('child_process').spawn;
const appveyor = require('tap-appveyor');
const merge = require('tap-merge');

let cmd = 'yarn test:coverage -- --tap';
if (process.env.include_spawn) {
  cmd = 'yarn test:spawn -- --tap & ' + cmd;
}
const child = spawn(cmd, { shell: true, stdio: ['pipe', 'pipe', 'inherit'] });
child.stdout.pipe(merge()).pipe(appveyor());
child.stdout.pipe(process.stdout);
child.on('close', code => code && (process.exitCode = code));
