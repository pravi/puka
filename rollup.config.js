import babel from 'rollup-plugin-babel';
import cleanup from 'rollup-plugin-cleanup';

export default {
  input: 'src/index.js',
  format: 'cjs',
  plugins: [
    babel({
      babelrc: false,
      plugins: [
        'external-helpers',
        'transform-class-properties',
        'transform-es2015-destructuring',
        'transform-es2015-parameters',
        'transform-es2015-spread',
      ],
    }),
    cleanup({
      comments: /^(?:(?!eslint-)[^])*$/,
    }),
  ],
};
