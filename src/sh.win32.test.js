import test from 'ava';
import { Formatter } from './Formatter';
import { sh } from './sh';
import { unquoted } from './unquoted';

// This file is for tests that exercise behaviors specific to win32.

Formatter.default = Formatter.for('win32');

test("translate semicolon to ampersand", t => {
  t.expect(sh`foo; bar`, "to be", 'foo& bar');
});

test("protect characters special on Windows only", t => {
  t.expect(sh`script %PATH%`, "to be", 'script ^%PATH^%');
  t.expect(sh`script "%PATH%"`, "to be", 'script ^"^%PATH^%^"');
});

test("pipes add carets on the right", t =>
  t.expect(sh`foo | bar ${'"'}`,
    "to be", 'foo | bar ^^^"\\^^^"^^^"'));

test("pipes add carets on the left", t =>
  t.expect(sh`foo ${'"'} | bar`,
    "to be", 'foo ^^^"\\^^^"^^^" | bar'));

test("more pipes add more carets", t =>
  t.expect(sh`foo | (bar | baz ${'"'})`,
    "to be", 'foo | (bar | baz ^^^^^^^"\\^^^^^^^"^^^^^^^")'));

test("unquoted pipes count", t =>
  t.expect(sh`foo ${unquoted('|')} bar ${'"'}`,
    "to be", 'foo | bar ^^^"\\^^^"^^^"'));

test("careted pipes don't count", t =>
  t.expect(sh`foo ${unquoted('^|')} bar ${'"'}`,
    "to be", 'foo ^| bar ^"\\^"^"'));

test("quoted pipes don't count", t =>
  t.expect(sh`foo ${unquoted('"> | <"')} bar ${'"'}`,
    "to be", 'foo "> | <" bar ^"\\^"^"'));

test("sequence operators don't count", t =>
  t.expect(sh`foo || bar ${'"'}`,
    "to be", 'foo || bar ^"\\^"^"'));

test("unmatched parentheses don't ruin everything", t =>
  t.expect(sh`foo | bar); baz ${'"'}`,
    "to be", 'foo | bar)& baz ^"\\^"^"'));

test("redirects count", t =>
  t.expect(sh`foo | (bar > ${'%'} | baz)`,
    "to be", 'foo | (bar > ^^^^^^^% | baz)'));

test("redirecting stderr to stdout isn't a sequence operator", t =>
  t.expect(sh`foo % 2>&1 | bar`,
    "to be", 'foo ^^^% 2>&1 | bar'));
