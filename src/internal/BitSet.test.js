import test from 'ava';
import Generators from 'chance-generators';
import expect from 'unexpected';
import unexpectedCheck from 'unexpected-check';
import { BitSet } from './BitSet';

expect.use(unexpectedCheck);

expect.addAssertion("<number> to <number> <assertion>",
  (expect, from, to) => {
    expect.errorMode = 'bubble';
    for (let i = from; i < to; i++) {
      expect.shift(i);
    }
  });

expect.addAssertion("<array> to be equal", (expect, [l, r]) =>
  expect(l, "to be", r));

const gen = new Generators(0);

const bitSetEquivalentToSet = values => {
  const a = new BitSet();
  const b = new Set();
  for (const value of values) {
    a.add(value);
    b.add(value);
  }
  expect(0, "to", 100,
    "when passed as parameter to", i => [a.has(i), b.has(i)],
    "to be equal");
};

test("BitSet is equivalent to Set", t =>
  t.expect(bitSetEquivalentToSet, "to be valid for all",
    gen.array(gen.natural({ max: 89 }))));
