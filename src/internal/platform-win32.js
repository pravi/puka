import { execFrom, sticky } from './regex-utils';

export function quoteForCmd(text, forceQuote, caretDepth = 0) {
  // See the below blog post for an explanation of this function and
  // quoteForWin32:
  // eslint-disable-next-line max-len
  // https://blogs.msdn.microsoft.com/twistylittlepassagesallalike/2011/04/23/everyone-quotes-command-line-arguments-the-wrong-way/
  if (!text.length) {
    return '""';
  }
  if (/[\n\r]/.test(text)) {
    throw new Error("Line breaks can't be quoted on Windows");
  }
  const caretEscape = /["%]/.test(text);
  text = quoteForWin32(
    text, forceQuote || !caretEscape && /[&()<>^|]/.test(text));
  if (caretEscape) {
    // See Win32Context for explanation of what caretDepth is for.
    do {
      text = text.replace(/[\t "%&()<>^|]/g, '^$&');
    } while (caretDepth--);
  }
  return text;
}

export const quoteForWin32 =
  (text, forceQuote) =>
    forceQuote || /[\t "]/.test(text)
      ? `"${text.replace(/\\+(?=$|")/g, '$&$&').replace(/"/g, '\\"')}"`
      : text;

export const cmdMetaChars = /[\t\n\r "%&()<>^|]/;

export class Win32Context {
  currentScope = newScope(null)
  scopesByObject = new Map()
  read(text) {
    // When cmd.exe pipes to or from a batch file, it spawns a second copy of
    // itself to run the inner command. This necessitates doubling up on carets
    // so that escaped characters survive both cmd.exe invocations. See:
    // eslint-disable-next-line max-len
    // https://stackoverflow.com/questions/8192318/why-does-delayed-expansion-fail-when-inside-a-piped-block-of-code#8194279
    // https://ss64.com/nt/syntax-redirection.html
    //
    // Parentheses can create an additional subshell, requiring additional
    // escaping... it's a mess.
    //
    // So here's what we do about it: we read all unquoted text in a shell
    // string and put it through this tiny parser that looks for pipes,
    // sequence operators (&, &&, ||), and parentheses. This can't be part of
    // the main Puka parsing, because it can be affected by `unquoted(...)`
    // values provided at evaluation time.
    //
    // Then, after associating each thing that needs to be quoted with a scope
    // (via `mark()`), we can determine the depth of caret escaping required
    // in each scope and pass it (via `Formatter::quote()`) to `quoteForCmd()`.
    //
    // See also `ShellStringText`, which holds the logic for the previous
    // paragraph.

    const { length } = text;
    for (let pos = 0, match; pos < length;) {
      if (match = execFrom(reUnimportant, text, pos)) {
        pos += match[0].length;
      }
      if (pos >= length) break;
      if (match = execFrom(reSeqOp, text, pos)) {
        this.seq();
        pos += match[0].length;
      } else {
        const char = text.charCodeAt(pos);
        if (char === CARET) {
          pos += 2;
        } else if (char === QUOTE) {
          // If you were foolish enough to leave a dangling quotation mark in
          // an unquoted span... you're likely to have bigger problems than
          // incorrect escaping. So we just do the simplest thing of looking for
          // the end quote only in this piece of text.
          pos += execFrom(reNotQuote, text, pos + 1)[0].length + 2;
        } else {
          if (char === OPEN_PAREN) {
            this.enterScope();
          } else if (char === CLOSE_PAREN) {
            this.exitScope();
          } else { // (char === '|')
            this.currentScope.depthDelta = 1;
          }
          pos++;
        }
      }
    }
  }
  enterScope() {
    this.currentScope = newScope(this.currentScope);
  }
  exitScope() {
    this.currentScope = this.currentScope.parent
      || (this.currentScope.parent = newScope(null));
  }
  seq() {
    // | binds tighter than sequence operators, so the latter create new sibling
    // scopes for future |s to mutate.
    this.currentScope = newScope(this.currentScope.parent);
  }
  mark(obj) {
    this.scopesByObject.set(obj, this.currentScope);
  }
  at(obj) {
    return { depth: getDepth(this.scopesByObject.get(obj)) };
  }
}

const getDepth = scope => scope === null ? 0
  : scope.depth !== -1 ? scope.depth
  : scope.depth = getDepth(scope.parent) + scope.depthDelta;

const newScope = parent => ({ parent, depthDelta: 0, depth: -1 });

const CARET = '^'.charCodeAt();
const QUOTE = '"'.charCodeAt();
const OPEN_PAREN = '('.charCodeAt();
const CLOSE_PAREN = ')'.charCodeAt();

const reNotQuote = sticky('[^"]*');
const reSeqOp = sticky('&&?|\\|\\|');
const reUnimportant = sticky('(?:>&|[^"$&()^|])+');
