import test0 from 'ava';
import { quoteForWin32 } from './platform-win32';

// Workaround for ava not matching titleless tests; remove when ava >0.22.0
const test = (f, ...args) => typeof f === 'function'
  ? test0('', f, ...args)
  : test0(f, ...args);

test(t =>
  t.expect(quoteForWin32('hello"world'), "to be", '"hello\\"world"'));

test(t =>
  t.expect(quoteForWin32('hello""world'), "to be", '"hello\\"\\"world"'));

test(t =>
  t.expect(quoteForWin32('hello\\world'), "to be", 'hello\\world'));

test(t =>
  t.expect(quoteForWin32('hello\\\\world'), "to be", 'hello\\\\world'));

test(t =>
  t.expect(quoteForWin32('hello\\"world'), "to be", '"hello\\\\\\"world"'));

test(t =>
  t.expect(quoteForWin32('hello\\\\"world'),
    "to be", '"hello\\\\\\\\\\"world"'));

test(t =>
  t.expect(quoteForWin32('hello world\\'), "to be", '"hello world\\\\"'));
