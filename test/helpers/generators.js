import generators from 'chance-generators';

export const gen = generators(0);

const symbolicRanges = [
  { min: 0x01, max: 0x2F },
  { min: 0x3A, max: 0x40 },
  { min: 0x5B, max: 0x60 },
  { min: 0x7B, max: 0x7F },
];

const symbolicCodePoints = gen.weighted(
  symbolicRanges.map(r => gen.natural(r)),
  symbolicRanges.map(r => r.max - r.min + 1));

const anyCharacter =
  gen.weighted([
    symbolicCodePoints,
    gen.natural({ min: 0x01, max: 0x7F }),
    gen.natural({ min: 0x80, max: 0xD7FF }),
    gen.natural({ min: 0x100000, max: 0x10FFFD }),
  ], [6, 1, 1, 1]).map(n => String.fromCodePoint(n));

export const specialStrings = [
  '\n', '\r', '\t', '\f', '\v', ' ', '"', '#', '$X', '%CD%', '!CD!', '&', "'",
  '(', ')', '*', '?', ';', '<', '>', '\\', '\\"', '^', '|', '~', '!', ':',
  '\u2029'];

const specialString = gen.pickone(specialStrings);

export const trickyString =
  gen.n(
    gen.weighted([specialString, anyCharacter], [3, 1]),
    gen.natural({ max: 10 }))
    .map(arr => arr.join(''));
